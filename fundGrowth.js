const R = require('ramda');
const RD = require('ramda-decimal');
const YM = require('year-month');

const DATE_FORMAT = "2006-01";
const NUM_DECIMAL_PLACES = 6;
const ONE_HUNDRED = 100;

const getGrowAmountFn = (growthRate) => {
  const percentageGrowthRate = R.compose(
    RD.divideBy(ONE_HUNDRED),
    RD.add(ONE_HUNDRED)
  )(growthRate);

  return RD.multiply(percentageGrowthRate);
};

const projectFund = (initialFund, projectionMonths) => {
  const projection = [];
  const { monthlyContribution, monthlyExpenditure } = initialFund;

  const growAmountFn = getGrowAmountFn(initialFund.growthRate);
  let currentAmount = initialFund.amount;
  let currentDate = YM.parse(initialFund.date);


  for (let i = 1; i <= projectionMonths; i += 1) {
    currentDate = currentDate.addMonths(1);
    currentAmount = R.compose(
      growAmountFn,
      RD.add(monthlyContribution),
      RD.subtract(currentAmount)
    )(monthlyExpenditure);

    projection.push({
      amount: currentAmount,
      date: currentDate.toString(),
    });
  }

  return {
    ...initialFund,
    projection,
  };
};

module.exports = {
  projectFund,
};
