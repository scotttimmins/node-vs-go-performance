package fundGrowth

import "testing"

func BenchmarkProjectFund(b *testing.B) {
	// Setup
	initialFund := loadInitialFund("./initial-fund.json")
	b.ResetTimer()

	projectFund(*initialFund, b.N)
	// projectedFund := projectFund(*initialFund, b.N)
	// prettyPrintJson(projectedFund)
}
