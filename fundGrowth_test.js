const { hrtime } = require('process');
const {
	projectFund,
} = require('./fundGrowth');

const NUM_ITERATIONS = 1000000;

const BenchmarkProjectFund = () => {
	// Setup
	const initialFund = require('./initial-fund.json');

	const startTime = hrtime.bigint();

	const projectedFund = projectFund(initialFund, NUM_ITERATIONS)
	const endTime = hrtime.bigint();

	console.log(JSON.stringify(projectedFund, null, 2));

	const nanoseconds = endTime - startTime;
	console.log(`Number of months projected: ${NUM_ITERATIONS}            ${nanoseconds / BigInt(NUM_ITERATIONS)}ns per monthly projectioon`);
}

BenchmarkProjectFund();
