package fundGrowth

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/big"
	"os"
	"time"
)

var DATE_FORMAT string = "2006-01"
var NUM_DECIMAL_PLACES int = 6
var ONE_HUNDRED = big.NewFloat(100)

type InitialFund struct {
	Name                string    `json:"name"`
	StartDate           string    `json:"date"`
	Amount              big.Float `json:"amount"`
	GrowthRate          big.Float `json:"growthRate"`
	MonthlyContribution big.Float `json:"monthlyContribution"`
	MonthlyExpenditure  big.Float `json:"monthlyExpenditure"`
}

type FundAmount struct {
	Date   string    `json:"date"`
	Amount big.Float `json:"amount"`
}

func (fundAmount *FundAmount) MarshalJSON() ([]byte, error) {
	type Alias FundAmount
	return json.Marshal(&struct {
		Amount string `json:"amount"`
		*Alias
	}{
		Amount: fundAmount.Amount.Text('f', NUM_DECIMAL_PLACES),
		Alias:  (*Alias)(fundAmount),
	})
}

type ProjectedFund struct {
	InitialFund
	Projection []FundAmount `json:"projection"`
}

func prettyPrintJson(data interface{}) {
	var jsonData []byte
	jsonData, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(jsonData))
}

func loadInitialFund(filename string) *InitialFund {
	jsonFile, err := os.Open(filename)

	if err != nil {
		fmt.Println(err)
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)

	initialFund := new(InitialFund)
	json.Unmarshal(byteValue, initialFund)

	jsonFile.Close()

	return initialFund
}

func getGrowAmountFn(growthRate big.Float) func(big.Float) big.Float {
	adjustedGrowthRate := new(big.Float).Add(&growthRate, ONE_HUNDRED)
	percentageGrowthRate := new(big.Float).Quo(adjustedGrowthRate, ONE_HUNDRED)

	return func(amount big.Float) big.Float {
		return *new(big.Float).Mul(&amount, percentageGrowthRate)
	}
}

func projectFund(initialFund InitialFund, projectionMonths int) *ProjectedFund {
	var projection []FundAmount
	growAmountFn := getGrowAmountFn(initialFund.GrowthRate)
	currentAmount := initialFund.Amount
	currentDate, err := time.Parse(DATE_FORMAT, initialFund.StartDate)
	if err != nil {
		fmt.Println(err)
	}

	for i := 1; i <= projectionMonths; i++ {
		currentDate = currentDate.AddDate(0, 1, 0)
		currentAmount = *(new(big.Float).Add(&currentAmount, &initialFund.MonthlyContribution))
		currentAmount = *(new(big.Float).Sub(&currentAmount, &initialFund.MonthlyExpenditure))
		currentAmount = growAmountFn(currentAmount)

		fundAmount := new(FundAmount)
		fundAmount.Date = currentDate.Format(DATE_FORMAT)
		fundAmount.Amount = currentAmount

		projection = append(projection, *fundAmount)
	}

	var projectedFund ProjectedFund
	projectedFund.InitialFund = initialFund
	projectedFund.Projection = projection
	return &projectedFund
}

func main() {
	initialFund := loadInitialFund("./initial-fund.json")
	projectedFund := projectFund(*initialFund, 100)
	prettyPrintJson(projectedFund)
}
