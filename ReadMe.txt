To run the go benchmark the command is:
go test -bench BenchmarkProjectFund

To run the node benchmark the command is:
node fundGrowth_test.js
